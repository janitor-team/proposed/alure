alure (1.2-7) UNRELEASED; urgency=low

  * debian/control: Remove DM-Upload-Allowed.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 02 Nov 2012 19:38:35 +0100

alure (1.2-6) unstable; urgency=low

  * Fix build with GCC 4.7 by cherrypicking upstream commit 
    38e56406f47ee1322464e67b8fea8822856af61b
    (Closes: #674383)

 -- Moritz Muehlenhoff <jmm@debian.org>  Sun, 27 May 2012 12:36:22 +0200

alure (1.2-5) unstable; urgency=low

  * Change Depends on dlopened libraries to Recommends.
    This uses different mechanism to generate Recommends. (Closes: #669520)

 -- Andres Mejia <amejia@debian.org>  Sat, 05 May 2012 16:14:42 -0400

alure (1.2-4) unstable; urgency=low

  * Enable hardened compiler flags.

 -- Andres Mejia <amejia@debian.org>  Mon, 19 Mar 2012 09:52:02 -0400

alure (1.2-3) unstable; urgency=low

  * Allow dev package to be multiarch installable.
  * Bump to Standards-Version 3.9.3.

 -- Andres Mejia <amejia@debian.org>  Fri, 16 Mar 2012 16:40:00 -0400

alure (1.2-2) unstable; urgency=low

  * Be slightly less strict on version build depends on debhelper.
  * Update Vcs-* entries.
  * Create new package to install utility programs.
  * Support parallel builds.
  * Use -O2 instead of -O3.
  * Make lintian override comment more descriptive.
  * Set option to abort on upstream changes.

 -- Andres Mejia <amejia@debian.org>  Sun, 25 Sep 2011 12:12:27 -0400

alure (1.2-1) unstable; urgency=low

  [ Andres Mejia ]
  * Update to my @debian.org email.
  * Add gbp.conf to add pristine-tar option on by default.
  * Get rid of unneccesary lintian overrides.
  * Add DEP-3 patch descriptions and refresh patches.

  [ Tobias Hansen ]
  * New upstream release.
  * Add multiarch support.
  * Add man pages for example programs.

 -- Andres Mejia <amejia@debian.org>  Mon, 01 Aug 2011 10:25:56 -0400

alure (1.1-3) unstable; urgency=low

  * Add linker version script to prevent
    unnecessary symbols from being exported.
  * Add symbols file. (Closes: #625248)
  * Link to libdumb.so.1 instead of libdumb.so

 -- Tobias Hansen <tobias.han@gmx.de>  Sat, 28 May 2011 17:30:12 +0200

alure (1.1-2) unstable; urgency=low

  * Remove shlibs file. Dependency on libalure need not be so strict.

 -- Andres Mejia <mcitadel@gmail.com>  Fri, 22 Apr 2011 18:56:24 -0400

alure (1.1-1) unstable; urgency=low

  [ Tobias Hansen ]
  * Add shlibs file.
  * Add copyright information for Debian packaging.
  * Delete lintian-overrides for dbg package.
  * Unapply patches after build

  [ Andres Mejia ]
  * Enable modplug support without sndfile.h conflict.
  * Include dependencies for dynamically loaded libraries.
  * Build libalure for release. Removes debugging package.
  * Use compiler flags changes from openal-soft.
  * Bump Standards-Version to 3.9.2.

 -- Andres Mejia <mcitadel@gmail.com>  Fri, 22 Apr 2011 18:15:53 -0400

alure (1.0-2) unstable; urgency=low

  * Added fix for 64-bit builds taken from upstream.
  * Bump Standards-Version to 3.8.2.
  * Use DEB_CXXFLAGS instead of DEB_CFLAGS to override CXXFLAGS used.

 -- Andres Mejia <mcitadel@gmail.com>  Thu, 06 Aug 2009 22:02:13 -0400

alure (1.0-1) unstable; urgency=low

  * Initial release. (Closes: #532690)

 -- Andres Mejia <mcitadel@gmail.com>  Sat, 13 Jun 2009 17:30:05 -0400
