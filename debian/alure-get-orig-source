#!/bin/sh

# This script is used to download the upstream source for alure and
# generate it into an orig source tarball for Debian.

# Common variables used to ease maintenance of this script
ALURE_TARBALL="alure-1.2.tar.bz2"
ALURE_TARBALL_CHECKSUM="3088aba074ad02d95ea51e705053b9f5"
ALURE_VERSION="1.2"

USAGE="\n\
This script is used to generate the orig tarball used in building\n\
Debian packages for alure-$ALURE_VERSION.\n\
Usage: alure-get-orig-source [OPTION]\n\
\n\
 -h, --help                 Display this help message.\n\
 --remove-upstream-tarball  Remove the upstream source tarball.\n"

while [ "$#" -gt "0" ]
do
    case "$1" in
        -h|--help|*)
            echo "${USAGE}"
            exit 1
            ;;
    esac
done

make_current_tarball() {
    # Download the tarball if it's not available in the current directory
    [ -f $ALURE_TARBALL ] || \
        wget -c http://kcat.strangesoft.net/alure-releases/$ALURE_TARBALL

    # Verify the checksum
    COMPUTED_CHECKSUM=`md5sum $ALURE_TARBALL | cut -d ' ' -f 1`
    if [ $ALURE_TARBALL_CHECKSUM != $COMPUTED_CHECKSUM ] ; then
        echo "Checksum verification failed. Checksum was $COMPUTED_CHECKSUM
    Expected checksum $ALURE_TARBALL_CHECKSUM."
        exit 1
    else
        echo "Checksum verified. Checksum is $COMPUTED_CHECKSUM."
    fi

    echo -n "Renaming to alure_$ALURE_VERSION.orig.tar.bz2..."
    mv $ALURE_TARBALL alure_$ALURE_VERSION.orig.tar.bz2
    echo "done."
}

make_current_tarball
